'use strict';

const Audit = require('lighthouse').Audit;

const MAX_API_TIME = 3000;

class APIAudit extends Audit {
    static get meta() {
        return {
            category: 'MyPerformance',
            name: 'api-audit',
            description: 'First API response ready.',
            failureDescription: 'First API call slow to respond.',
            helpText: 'Used to measure time from navigationStart to when the API' +
            ' responds its firts call.',
            requiredArtifacts: ['TimeToAPIResponse']
        };
    }

    static audit(artifacts) {
        const loadedTime = artifacts.TimeToAPIResponse;

        const belowThreshold = loadedTime <= MAX_API_TIME;

        return {
            rawValue: loadedTime,
            score: belowThreshold
        };
    }
}

module.exports = APIAudit;
